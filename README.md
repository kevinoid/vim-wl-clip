## Deprecated

This project is no longer being actively maintained and its use is
discouraged.  [vim-fakeclip](https://github.com/kana/vim-fakeclip) with
[kana/vim-fakeclip#32](https://github.com/kana/vim-fakeclip/pull/32) supports
many more commands, command repetition, and configurability.


Wayland Clipboard Key Mappings for Vim
======================================

A [Vim](https://www.vim.org/) plugin which provides [key
mapping](https://vimhelp.org/map.txt.html)s for some operations on the
[x11-selection](https://vimhelp.org/gui_x11.txt.html#x11-selection)
using [wl-clipboard](https://github.com/bugaevc/wl-clipboard) for
[Wayland](https://wayland.freedesktop.org/) instead of X11.

**Note:** This plugin may not be required if
[XWayland](https://wayland.freedesktop.org/xserver.html) is running.  Most
Wayland compositors synchronize the Wayland and XWayland selections.  However,
security restrictions may prevent synchronization in some cases (e.g. [wlroots
denies access when Vim is run in a Wayland-native
terminal](https://github.com/swaywm/wlroots/pull/487/commits/b884025558e750268a06818dc63bc46716c75843)).

**Note:** It is hoped that this plugin will be superseded by native Wayland
support in Vim.  See [vim/vim#5157](https://github.com/vim/vim/issues/5157).


## Installation

This plugin can be installed in the usual ways:

### Using [Vim Packages](https://vimhelp.org/repeat.txt.html#packages)

```sh
git clone https://gitlab.com/kevinoid/vim-wl-clip.git ~/.vim/pack/git-plugins/start/vim-wl-clip
```

### Using [Pathogen](https://github.com/tpope/vim-pathogen)

```sh
git clone https://gitlab.com/kevinoid/vim-wl-clip.git ~/.vim/bundle/vim-wl-clip
```

### Using [Vundle](https://github.com/VundleVim/Vundle.vim)

Add the following to `.vimrc`:
```vim
Plugin 'https://gitlab.com/kevinoid/vim-wl-clip.git'
```
Then run `:PluginInstall`.

### Using [vim-plug](https://github.com/junegunn/vim-plug)

Add the following to `.vimrc` between `plug#begin()` and `plug#end()`:
```vim
Plug 'https://gitlab.com/kevinoid/vim-wl-clip.git'
```


## License

You can redistribute and/or modify this software under the terms of the
[MIT License](LICENSE-MIT.txt) or (at your option) the [VIM
License](LICENSE-VIM.txt).
