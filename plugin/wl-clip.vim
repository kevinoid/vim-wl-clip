" Key mappings for x11-selection operations on Wayland using wl-clipboard.
" Last Change: 2020-04-30
" Maintainer:  Kevin Locke <kevin@kevinlocke.name>
" License:     MIT License or (at your option) Vim License

if exists('g:loaded_wl_clip')
  finish
endif
let g:loaded_wl_clip = 1

function! WlClipGet(wl_selection)
    " Prepare wl-paste arguments
    let args = ' --no-newline'
    if a:wl_selection ==# 'primary'
        let args .= ' --primary'
    endif

    " Get contents from wl-paste
    let wl_paste_output = system('wl-paste' . args)

    if v:shell_error != 0
        " wl-paste failed

        if wl_paste_output ==# "No selection\n"
            " Error was due to no selection (i.e. nothing in clipboard)
            let wl_paste_output = ''
        else
            " Error was not due to empty selection, throw it
            throw wl_paste_output
        endif
    endif

    " Applications are inconsistent about including or adding \r to clipboard
    " content and whether to normalize it on paste.
    " See https://gitlab.gnome.org/GNOME/gtk/issues/2307
    "
    " Pasting \r doesn't appear to be desirable in most cases anyway.  With
    " ff=dos, pasting content with \r\n results in ^M at end of line which
    " saves as \r\r\n.
    "
    " Normalize end-of-line markers, unless configured not to.
    if get(g:, 'wl_clip_normalize_eol', 1)
        let wl_paste_output = substitute(wl_paste_output, '\r\n', '\n', 'g')
    endif

    return wl_paste_output
endfunction

function! WlClipPaste(wl_selection)
    " Save unnamed register
    let reg_save = @@

    try
        " Set unnamed register contents from wl-paste
        let @@ = WlClipGet(a:wl_selection)

        " Paste unnamed register
        silent exe 'normal! p'
    finally
        " Restore unnamed register
        let @@ = reg_save
    endtry
endfunction

function! WlClipSet(wl_selection, motion)
    " Save selection option and unnamed register
    let sel_save = &selection
    let reg_save = @@

    " Include the last character of the selection
    let &selection = 'inclusive'

    " Yank selection or motion into unnamed register
    if a:motion ==# 'visual'
      silent exe 'normal! gvy'
    elseif a:motion ==# 'line'
      silent exe "normal! '[V']y"
    else
      silent exe 'normal! `[v`]y'
    endif

    " Prepare wl-copy arguments
    let args = ''
    if a:wl_selection ==# 'primary'
        let args .= ' --primary'
    endif

    " Send unnamed register contents to wl-copy
    let wl_copy_output = system('wl-copy' . args, @@)

    " Restore selection option and unnamed register
    let &selection = sel_save
    let @@ = reg_save

    " If wl-copy failed, throw its error message
    if v:shell_error != 0
        throw wl_copy_output
    endif
endfunction

function! WlClipSetClipboard(motion)
    call WlClipSet('clipboard', a:motion)
endfunction

function! WlClipSetPrimary(motion)
    call WlClipSet('primary', a:motion)
endfunction

if exists('g:wl_clip_enabled')
    " Allow users to enable/disable mapping using a global variable
    let s:do_mapping = g:wl_clip_enabled
else
    " By default, enable mappings when clipboard support is not present (so
    " no risk of breaking usable clipboard), or running under Wayland (i.e.
    " $WAYLAND_DISPLAY is non-empty) and wl-copy executable is available.
    let s:do_mapping = !has('clipboard')
    \ || (!empty($WAYLAND_DISPLAY) && executable('wl-copy'))
endif

if s:do_mapping
    " Add mappings to use wl-clipboard instead of built-in gui-selections.

    " Map yank operator
    " TODO: Map cCdDrRsSxXY in addition to y
    if has('eval')
        " Note: See :help :map-operator for mapping motion commands as below.
        " FIXME: Does not work with "+yy since y is not a motion
        nnoremap <silent> "+y :set opfunc=WlClipSetClipboard<CR>g@
        nnoremap <silent> "*y :set opfunc=WlClipSetPrimary<CR>g@
    endif
    vnoremap <silent> "+y :<C-U>call WlClipSetClipboard('visual')<CR>
    vnoremap <silent> "*y :<C-U>call WlClipSetPrimary('visual')<CR>

    " Map paste and insert operators
    " TODO: Map P, gp, gP in addition to p
    nnoremap <silent> "+p :call WlClipPaste('clipboard')<CR>
    nnoremap <silent> "*p :call WlClipPaste('primary')<CR>
    noremap! <silent> <special> <C-R>+ <C-R>=WlClipGet('clipboard')<CR>
    noremap! <silent> <special> <C-R>* <C-R>=WlClipGet('primary')<CR>
endif
